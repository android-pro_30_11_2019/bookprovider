package com.example.bookprovider;

import androidx.appcompat.app.AppCompatActivity;
import androidx.loader.content.CursorLoader;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvBook;
    private EditText etId,etTitle,etisbn;
    private Button btnDelete,btnUpdate;

    private final Uri uri = Uri.parse("content://com.example.provider.books/books");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvBook = findViewById(R.id.tvBook);
        etId = findViewById(R.id.etId);
        etTitle = findViewById(R.id.etTitle);
        etisbn = findViewById(R.id.etisbn);

        btnDelete = findViewById(R.id.btnDelete);
        btnUpdate = findViewById(R.id.btnUpdate);

        //apply lister;
        btnDelete.setOnClickListener(this);
        btnUpdate.setOnClickListener(this);

        tvBook.setText(ReadBook());
    }
    private String ReadBook() {

        Cursor cursor;
        CursorLoader loader = new CursorLoader(
                this,
                uri,
                null,
                null,
                null,
                null
        );
        cursor = loader.loadInBackground();
        String str = "";

        while(cursor != null && cursor.moveToNext()){
            String id = cursor.getString(cursor.getColumnIndex("_id"));
            String title = cursor.getString(cursor.getColumnIndex("title"));
            String isbn = cursor.getString(cursor.getColumnIndex("isbn"));

            str = "{ "+id+ ","+title + "," + isbn+"}";
        }
        return str;
    }

    @Override
    public void onClick(View v) {
        String id = etId.getText().toString();
        String title = etTitle.getText().toString();
        String isbn = etisbn.getText().toString();

        switch (v.getId()){
            case R.id.btnDelete:
                deleteBookProvider(id);
                break;
            case R.id.btnUpdate:
                updateBookProvider(id,title,isbn);
                break;

        }
    }

    private void updateBookProvider(String id, String title, String isbn) {
        ContentValues values = new ContentValues();
        values.put("title",title);
        values.put("isbn",isbn);

        int rowUpdate = getContentResolver().update(uri,values,"_id=?",new String[]{id});
        if(rowUpdate>0)
            Toast.makeText(this, "Items was Updated!", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(this, "Something went wrong, your items wasn't Updated.", Toast.LENGTH_SHORT).show();
    }

    private void deleteBookProvider(String id) {
        int rowDelete = getContentResolver().delete(uri,"_id=?",new String[]{id});
        if(rowDelete>0){
            Toast.makeText(this, "Items was deleted!", Toast.LENGTH_SHORT).show();
        }else
            Toast.makeText(this, "Something went wrong, your items wasn't deleted.", Toast.LENGTH_SHORT).show();
    }
}
